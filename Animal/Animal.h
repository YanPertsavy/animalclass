#pragma once
#include <iostream>
#include <string>
using namespace std;

class Animal
{
public:
	Animal()
	{
	}
	Animal(string,double,string, string, string, string, string, string);
	void print();
	double setLifetime(double n);

private:
	string name;
	double lifetime;
	string type;
	string animalClass;
	string animalGroup;
	string family;
	string genus;
	string species;
};

