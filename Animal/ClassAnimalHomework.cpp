﻿#include <iostream>
#include <string>
#include "Animal.h";
using namespace std;

int main()
{
    const int length = 1;
    Animal animals[length];
    animals[0] = Animal("cat",1.0,"a","b","c","d","e","f");
    animals[0].print();
    animals[0].setLifetime(2.5);
    cout << endl;
    animals[0].print();
}

