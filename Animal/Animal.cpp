#include "Animal.h"
#include <iostream>
#include <string>
using namespace std;

Animal::Animal(string name, double lifetime, string type, string animalClass, string animalGroup, string family, string genus, string species)
{
	this->name = name;
	this->lifetime = lifetime;
	this->type = type;
	this->animalClass = animalClass;
	this->animalGroup = animalGroup;
	this->family = family;
	this->genus = genus;
	this->species = species;
}

void Animal::print()
{
	cout << "Name: " << name << " lifetime: "<< lifetime<<" type: "<< type << " animalClass: " << animalClass << " animalGroup: " << animalGroup << " family: " << family << " genus: " << genus << " species: " << species;
}

double Animal::setLifetime(double n)
{
	 return this->lifetime = n;
}
